# shellme64

Desc: `Same concept, more bytes`

Architecture: x64

Given files:

* shellme64

Hints:

* Just like x86 with some different registers.

Flag: `TUCTF{54m3_5h3llc0d3,_ju57_m0r3_by735}`
